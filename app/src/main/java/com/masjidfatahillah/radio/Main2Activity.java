package com.masjidfatahillah.radio;

import android.app.AlertDialog;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.liangfeizc.RubberIndicator;

public class Main2Activity extends AppCompatActivity {

    ViewPager viewPager;
//    private RubberIndicator mRubberIndicator;
    private int posLama = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new RadioFragmentAdapter(getSupportFragmentManager()));

        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            String[] types = codecInfo.getSupportedTypes();
            System.out.println((i + 1));
            for(int j=0; j<types.length; j++) {
                System.out.println(types[j]);
            }
        }

//        mRubberIndicator = (RubberIndicator) findViewById(R.id.home_fragment_rubber_indicator);
//        mRubberIndicator.setCount(1);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                System.out.println("position: "+position);
            }

            @Override
            public void onPageSelected(int posBaru) {
//                if (mRubberIndicator.getFocusPosition() == 1 && posBaru > posLama) {
//                    mRubberIndicator.setFocusPosition(posBaru % 2);
//                    mRubberIndicator.setCount(3);
//                    mRubberIndicator.setCount(2);
//                } else if (mRubberIndicator.getFocusPosition() == 0 && posBaru < posLama) {
//                    mRubberIndicator.setFocusPosition(posBaru % 2);
//                    mRubberIndicator.setCount(2 + 1);
//                    mRubberIndicator.setCount(2);
//                } else if (posBaru > posLama) {
//                    mRubberIndicator.moveToLeft();
//                } else {
//                    mRubberIndicator.moveToRight();
//                }
                posLama = posBaru;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
//                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
//                    stopSwipe();
//                    startSwipe();
//                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            TextView msg = new TextView(this);
            msg.setMovementMethod(LinkMovementMethod.getInstance());
            msg.setClickable(true);
            msg.setTextSize(18);
            msg.setPadding(20, 20, 20, 20);
            msg.setText(Html.fromHtml("بسم الله الرحمن الرحيم <br/><br/>" +
                    "</br>Dengan mengharap Ridha Allah Ta'ala semata," +
                    "<br/><br/>Aplikasi ini  merupakan sarana penyiaran" +
                    "<br/>Masjid Fatahillah, Depok di URL: " +
                    "<a href=\"http://103.28.148.220:9992/listen.pls\">http://103.28.148.220:9992/listen.pls</a>" +
                    "<br/><br/>Materi yang disiarkan melalui aplikasi ini termasuk:\n" +
                    "<br/>- Taklim Harian" +
                    "<br/>- Dauroh" +
                    "<br/>- Murottal" +
                    "<br/><br/>Radio streaming ini juga disiarkan di:\n" +
                    "<a href=\"http://masjidfatahillah.com\">Masjid Fatahillah</a>" +
                    "<br/><br/>Barokallahu fiikum," +
                    "<br/><br/>Masjid Fatahillah Depok" +
                    "<br/>Jl. Fatahillah II/33, Tanah Baru, Beji, Depok 16426."));
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Tentang Aplikasi Ini");
            alertDialogBuilder.setView(msg);
            alertDialogBuilder.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class RadioFragmentAdapter extends FragmentPagerAdapter {

        MainFragment mainFragment = new MainFragment();
        DropboxFragment dropboxFragment = new DropboxFragment();

        public RadioFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0 )
                return mainFragment;
            return dropboxFragment;
        }

        @Override
        public int getCount() {
            return 1;
        }

    }

}
