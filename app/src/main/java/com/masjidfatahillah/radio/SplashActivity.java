package com.masjidfatahillah.radio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import java.io.File;

public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(5 * 1000);
                    startActivity(new Intent(getBaseContext(), Main2Activity.class));
                    File file = new File(Constants.FILE_DOWNLOAD);
                    if(!file.exists()) {
                        file.mkdirs();
                    }
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

}
