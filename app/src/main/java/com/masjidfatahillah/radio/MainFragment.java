package com.masjidfatahillah.radio;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.net.URL;

import at.markushi.ui.CircleButton;
import es.claucookie.miniequalizerlibrary.EqualizerView;

/**
 * Created by riorizkyrainey on 11/01/16.
 */
public class MainFragment extends Fragment implements View.OnClickListener{

    View root;
    private EqualizerView equalizerView;
    private CircleButton circleButton;
    private TextView dynamicTextView;
    private TextView dynamic2TextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_main, container, false);

        equalizerView = (EqualizerView) root.findViewById(R.id.equalizer_view);
        circleButton = (CircleButton) root.findViewById(R.id.button_play);
        dynamicTextView = (TextView) root.findViewById(R.id.titleDynamic);
        dynamic2TextView = (TextView) root.findViewById(R.id.titleDynamic2);
        circleButton.setOnClickListener(this);

        if (RadioService.isPlay) {
            playRadio();
        }
        else {
            stopRadio();
        }
        return root;
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), RadioService.class);
        if (equalizerView.isAnimating()) {
            stopRadio();
            getActivity().stopService(intent);
        } else {
            playRadio();
            Bundle extras = new Bundle();
            extras.putString("url", "http://103.28.148.220:9992");
            extras.putString("name", "Radio Kita");
            intent.putExtras(extras);
            getActivity().startService(intent);
        }
    }

    private void stopRadio() {
        dynamicTextView.setText("Radio Streaming");
        dynamic2TextView.setText("Masjid Fatahillah");
        circleButton.post(new Runnable() {
            @Override
            public void run() {
                circleButton.setImageResource(R.drawable.play);
            }
        });
        equalizerView.stopBars(); // When you want equalizer stops animating
    }

    private void playRadio() {
//        new GetTitle().execute();
        circleButton.post(new Runnable() {
            @Override
            public void run() {
                circleButton.setImageResource(R.drawable.pause);
            }
        });
        equalizerView.animateBars();
    }

    public class GetTitle extends AsyncTask<Void, Void, Void> {

        ParsingHeaderData.TrackData trackData;

        String title = "";
        String artist = "";

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL url = new URL(
                        "http://103.28.148.220:9992");

                IcyStreamMeta icyStreamMeta = new IcyStreamMeta(url);
                title = icyStreamMeta.getTitle();
                artist = icyStreamMeta.getArtist();
//                ParsingHeaderData streaming = new ParsingHeaderData();
//                trackData = streaming.getTrackDetails(url);
//                Log.e("Song Artist Name ", trackData.artist);
//                Log.e("Song Artist Title", trackData.title);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dynamicTextView.setText(title);
            dynamic2TextView.setText(artist);
        }
    }
}
