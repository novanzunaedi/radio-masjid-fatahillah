package com.masjidfatahillah.radio;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AppKeyPair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;

/**
 * Created by riorizkyrainey on 11/01/16.
 */
public class DropboxFragment extends Fragment {

    final static private String APP_KEY = "lwr3i3g3hxv9ra7";
    final static private String APP_SECRET = "vqp7q7xc2jvv2v0";

    String[] fnames = null;
    ArrayList<DropboxAPI.Entry> files;
    AndroidAuthSession session;

    View root;
    ListView list;


    private DropboxAPI<AndroidAuthSession> mDBApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        session = new AndroidAuthSession(appKeys);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);
//        session.startOAuth2Authentication(getActivity());
        root = inflater.inflate(R.layout.fragment_dropbox, container, false);
        list = (ListView) root.findViewById(R.id.list);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(session.authenticationSuccessful()) {
            DropboxAPI.Entry dirent = null;
            try {
                dirent = mDBApi.metadata("https://www.dropbox.com/sh/af2gg9eslovay0e/AADsrhSrNe7RtgcUY9Kpt4k5a", 1000, null, true, null);

                files = new ArrayList<DropboxAPI.Entry>();
                int i = 0;
                ArrayList<String> dir = new ArrayList<String>();
                for (DropboxAPI.Entry ent : dirent.contents) {
                    files.add(ent);// Add it to the list of thumbs we can choose from
                    //dir = new ArrayList<String>();
                    dir.add(new String(files.get(i++).path));
                }
                i = 0;
                fnames = dir.toArray(new String[dir.size()]);

                ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, fnames);
                list.setAdapter(ad);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        DropboxAPI.DropboxLink shareLink = null;
                        try {
                            DropboxAPI.Entry ent = files.get(position);
                            shareLink = mDBApi.share(ent.path);
                            new LoadingAsyncTask(getActivity(), (shareLink.url).replaceFirst("https://www", "https://dl"), ent.fileName());
                        } catch (DropboxException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                fnames = new String[0];
            }
        }
    }

    public static String downloadFile(String sUrl, String pathFile) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        File checkFile = new File(pathFile);
        if (checkFile.exists()) {
            System.out.println("sudah pernah download dan file exist");
            return "success";
        }
        System.out.println("downloading...");
        try {
            sUrl = sUrl.replace(" ", "%20");
            System.out.println("url: " + sUrl);
            java.net.URL url = new java.net.URL(sUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();
            System.out.println("length: " + fileLength);

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(pathFile);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) > 0) {
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
                ignored.printStackTrace();
            }

            if (connection != null)
                connection.disconnect();
        }
        return "success";
    }


    private class LoadingAsyncTask extends AsyncTask<Void, Double, Void> {

        private boolean success = false;
        private ProgressDialog progressDialog;
        private Context context;
        private String message = "Cannot login";
        private String url;
        private String name;

        public LoadingAsyncTask(Context context, String url, String name) {
            this.context = context;
            this.url = url;
            this.name = name;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Downloading.. Pleas wait...");
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //download content
            if("success".equals(downloadFile(url, Constants.FILE_DOWNLOAD+name))) {
                success = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            if(success) {
                Toast.makeText(context, "Download succes dan ada di folder Radio Fatahillah", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Something wrong, please check internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
